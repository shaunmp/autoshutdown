This is a very basic JavaFX app.

Upon launch, it adds a shutdown, with a delay of 20mins. A dialog pops up, allowing the user to abort or delay it (by up to 40mins).

It was made for a specific purpose and is extremely basic - but what's the harm in releasing it anyway?

Only works on Windows.