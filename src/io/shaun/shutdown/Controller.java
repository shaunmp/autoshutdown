package io.shaun.shutdown;

import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created by Shaun on 24/07/2015.
 */
public class Controller {

    public void ok(ActionEvent actionEvent) {
        System.exit(0);
    }

    public void abort(ActionEvent actionEvent) {
        try {
            Runtime.getRuntime().exec("shutdown /a");
            System.exit(1);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void delay(ActionEvent actionEvent) throws IOException {
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);

        Text text = new Text("Please select how long you would like to extend the time till shutdown by:");
        grid.add(text, 0, 1);

        ChoiceBox choiceBox = new ChoiceBox();
        choiceBox.getItems().addAll("10min", "20min", "30min", "40min");
        choiceBox.getSelectionModel().selectFirst();
        grid.add(choiceBox, 1, 1);

        Button button = new Button("Delay");
        HBox btnBox = new HBox(10);
        btnBox.setAlignment(Pos.CENTER);
        btnBox.getChildren().add(button);
        grid.add(btnBox, 0, 2);

        button.setOnAction(event -> {
            try {
                delayShutdown(choiceBox.getSelectionModel().getSelectedItem().toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        Scene scene = new Scene(grid, 600, 100);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.show();
    }

    private void delayShutdown(String amount) throws IOException {
        String numeral = amount.substring(0, 2);
        int time = Integer.parseInt(numeral);
        Runtime.getRuntime().exec("shutdown /a");
        int fullTime = (time*60) + 1200;
        Runtime.getRuntime().exec("shutdown /s /t " + fullTime);
        System.exit(0);
    }
}
